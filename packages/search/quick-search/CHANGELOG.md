# @atlaskit/quick-search

## 1.3.0
- [minor] Remove unecessary dependencies [3bd4dd8](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3bd4dd8)

## 1.2.0
- [minor] Add missing AkSearch legacy export [1b40786](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1b40786)

## 1.1.0
- [minor] Fix wrongly named legacy exports [e7baf6b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e7baf6b)

## 1.0.0
- [major] Extract quick-search from @atlaskit/navigation [dda7e32](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/dda7e32)
