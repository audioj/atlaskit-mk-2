# @atlaskit/global-search

## 3.0.1
- [patch] Simplify tests [d2437e9](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d2437e9)

## 3.0.0
- [major] Building blocks to support Confluence mode. "context" is a required prop now. [a5f1cef](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a5f1cef)

## 2.1.1
- [patch] Bumping dependency on avatar [ca55e9c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ca55e9c)

## 2.1.0
- [minor] Remove dependency on navigation. [0ae3355](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0ae3355)

## 2.0.1
- [patch] Added missing dependencies and added lint rule to catch them all [0672503](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0672503)

## 2.0.0
- [major] Bump to React 16.3. [4251858](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4251858)

## 1.7.0
- [minor] Show empty state when no results were found at all [398901a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/398901a)

## 1.6.0

## 1.5.0
- [minor] Show error state when searches fail [4fbbb29](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4fbbb29)

## 1.4.0
- [minor] Improve rendering of Jira issues [7f28452](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7f28452)

## 1.3.1
- [patch] Clean up CSS for examples [1b7ffce](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1b7ffce)

## 1.3.0
- [minor] Adds search attribution analytics for confluence search results. [2d73f50](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/2d73f50)

## 1.2.0
- [minor] Remove environment prop and replace with explicit service url overrides. [b5bd020](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b5bd020)

## 1.1.1
- [patch] Support rendering of Jira results [0381f03](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0381f03)

## 1.1.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 1.0.1
- [patch] Update atlaskit/theme version [679e68a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/679e68a)

## 1.0.0
- [major] First release of global-search [7fcd54e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7fcd54e)
