export {
  default as GlobalQuickSearch,
} from './components/GlobalQuickSearchWrapper';

export { Config } from './api/configureSearchClients';
