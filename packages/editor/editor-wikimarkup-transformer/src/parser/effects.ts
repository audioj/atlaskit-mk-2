import { colors } from '@atlaskit/theme';
import { Effect } from '../interfaces';

export const TEXT_COLOR_GREY = {
  name: 'color',
  attrs: { color: colors.N80 },
} as Effect;

export const TEXT_BOLD = {
  name: 'strong',
  attrs: {},
} as Effect;

export const TEXT_ITALIC = {
  name: 'emphasis',
  attrs: {},
} as Effect;
