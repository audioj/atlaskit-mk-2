import { ComponentClass } from 'react';
import styled from 'styled-components';
import ToolbarButtonDefault from '../../../../ui/ToolbarButton';

// tslint:disable-next-line:variable-name
export const TrashToolbarButton: ComponentClass<any> = styled(
  ToolbarButtonDefault,
)`
  width: 24px;
`;
