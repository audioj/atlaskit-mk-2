export interface UploadParams {
  collection?: string;
  fetchMetadata?: boolean;
  autoFinalize?: boolean;
}
