import styled from 'styled-components';

import { HTMLAttributes, ComponentClass } from 'react';

export const Wrapper: ComponentClass<HTMLAttributes<{}>> = styled.div`
  flex-shrink: 0;
`;
