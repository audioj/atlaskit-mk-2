# @atlaskit/smart-card

## 2.0.1
- [patch] Added missing dependencies and added lint rule to catch them all [0672503](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0672503)

## 2.0.0
- [major] Bump to React 16.3. [4251858](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4251858)

## 1.0.2
- [patch] fixed missing and inccorect versions of dependencies [7bfbb09](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7bfbb09)
- [patch] fix dependencies [0e57cde](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0e57cde)

## 1.0.1
- [patch] fix path for atkaskit in package.json [6ac9661](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6ac9661)

