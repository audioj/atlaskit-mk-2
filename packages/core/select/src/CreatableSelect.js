// @flow

import Creatable from 'react-select/lib/Creatable';
import createSelect from './createSelect';

export default createSelect(Creatable);
