// @flow

import AsyncCreatable from 'react-select/lib/AsyncCreatable';
import createSelect from './createSelect';

export default createSelect(AsyncCreatable);
