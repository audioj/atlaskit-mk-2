# NPS

React component for rendering an NPS survery dialog

[Documentation](https://atlaskit.atlassian.com/packages/core/nps)

## Installation

```sh
npm install @NAME@
```
