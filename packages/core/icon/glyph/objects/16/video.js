'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../../../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Objects16VideoIcon = function Objects16VideoIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><path d="M6 4h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2zm11.37 10.954c.285.138.63-.05.63-.343V9.39c0-.293-.345-.481-.63-.343L15 10.194v3.613l2.37 1.147zM6 9.99v4.018c0 .54.449.991 1.003.991h4.994A.99.99 0 0 0 13 14.01V9.99c0-.539-.449-.99-1.003-.99H7.003A.99.99 0 0 0 6 9.99z" fill="currentColor" fill-rule="evenodd"/></svg>' }, props));
};
exports.default = Objects16VideoIcon;