'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../../../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Objects24UserIcon = function Objects24UserIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><path d="M3 0h18a3 3 0 0 1 3 3v18a3 3 0 0 1-3 3H3a3 3 0 0 1-3-3V3a3 3 0 0 1 3-3zm9 11a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7zm0 9c2.985 0 5.46-.959 5.923-2.214A.997.997 0 0 0 18 17.4V13a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1v4.4c0 .137.028.267.077.386C6.54 19.041 9.015 20 12 20z" fill="currentColor" fill-rule="evenodd"/></svg>' }, props));
};
exports.default = Objects24UserIcon;