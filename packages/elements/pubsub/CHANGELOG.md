# @atlaskit/pubsub

## 2.0.1

## 2.0.0
- [major] Update to React 16.3. [8a96fc8](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8a96fc8)

## 1.1.0
- [minor] FS-1874 Move @atlassian/pubsub to @atlaskit/pubsub [92af7f7](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/92af7f7)
